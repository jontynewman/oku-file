<?php

namespace JontyNewman\Oku;

use JontyNewman\Oku\ResponseBuilderInterface;
use RangeException;
use ShrooPHP\Core\ImmutableArrayObject;
use ShrooPHP\Core\Request\Response;
use ShrooPHP\Core\Runnable;
use ShrooPHP\RESTful\Collection;
use ShrooPHP\RESTful\Collection\Traits\FileSystem;
use ShrooPHP\RESTful\Request\Handler;
use ShrooPHP\RESTful\Resource;
use ShrooPHP\Framework\Requests\Request;
use UnexpectedValueException;

/**
 * Functionality for assisting with persistence via a file system.
 */
class File
{
	/**
	 * Builds a response using the given builder and the file at the given path.
	 *
	 * @param \JontyNewman\Oku\ResponseBuilderInterface $builder The builder to
	 * use in order to build the response.
	 * @param string $path The path of the file to use in order to build the
	 * response.
	 * @param array|null $headers The headers to associate with the request (if
	 * any).
	 * @param int|null $buffer The size of the buffer to use in order to render
	 * the content of the response (or NULL to use the default).
	 * @param string|null $type The content type to associate with the response
	 * (or NULL to use an approximation).
	 * @return bool Whether or not the response was built.
	 */
	public static function build(
		ResponseBuilderInterface $builder,
		string $path,
		array $headers = null,
		int $buffer = null,
		string $type = null
	): bool {

		$built = false;
		$request = new Request('GET', $path);
		$handler = new Handler(self::toFileSystem($buffer, $type));

		if (!is_null($headers)) {
			$request->setHeaders(new ImmutableArrayObject($headers));
		}

		$response = $handler->respond($request);

		if (!is_null($response)) {

			self::buildFromResponse($builder, $response);
			$built = true;
		}

		return $built;
	}

	/**
	 * Converts the given request path to a relative file path.
	 *
	 * @param string $path The request path to convert
	 * @param int|null $max The maximum length of file names (if any).
	 * @param string|null $separator The separator to use between directories in
	 * the generated file path (or NULL to use the directory separator of the
	 * operating system).
	 * @param bool $fail Whether or not to force an encoding failure (for code
	 * coverage purposes).
	 * @return string The converted request path.
	 * @throws \UnexpectedValueException A valid file path could not be
	 * generated.
	 */
	public static function path(
		string $path,
		int $max = null,
		string $separator = null,
		bool $fail = false
	): string {

		$encoded = base64_encode($path);

		if ($fail || !is_string($encoded)) {
			throw new UnexpectedValueException("Failed to encode path '{$path}'");
		}

		$trimmed = rtrim($encoded, '=');

		if ('' === $trimmed) {
			throw new UnexpectedValueException("Could not reliably encode path '{$path}'");
		}

		if (!is_null($max) && strlen($trimmed) > $max) {
			$trimmed = self::split($trimmed, $max, $separator);
		}

		return $trimmed;
	}

	/**
	 * Converts the given name to the equivalent request path.
	 *
	 * @param string $encoded The name to convert.
	 * @return string The converted name.
	 * @throws \UnexpectedValueException The given name could not be reliably
	 * decoded.
	 */
	public static function resolve(string $encoded): string
	{
		$decoded = base64_decode($encoded, true);

		if (false === $decoded) {
			throw new UnexpectedValueException("Could not reliably decode name '{$encoded}'");
		}

		return $decoded;
	}

	/**
	 * Modifies the given builder using the given response.
	 *
	 * @param \JontyNewman\Oku\ResponseBuilderInterface $builder The builder to
	 * modify.
	 * @param \ShrooPHP\Core\Response $response The response to use in order to
	 * modify the builder.
	 */
	private static function buildFromResponse(
		ResponseBuilderInterface $builder,
		Response $response
	): void {

		$builder->code($response->code());
		$builder->headers($response->headers());
		$builder->content(self::toCallable($response->content()));
	}

	/**
	 * Converts the given content to a callable.
	 *
	 * @param \ShrooPHP\Core\Runnable|null $content The content to convert.
	 * @return callable|null The converted content (if any).
	 */
	private static function toCallable(?Runnable $content): ?callable
	{
		if (!is_null($content)) {
			$content = function () use ($content) {
				$content->run();
			};
		}

		return $content;
	}

	/**
	 * Splits the given path into multiple directories.
	 *
	 * @param string $path The path to split.
	 * @param int $length The length of directory names.
	 * @param string|null $separator The separator to use in order to separate
	 * directories in the path (or NULL to use the directory separator of the
	 * operating system).
	 * @return string The split path.
	 * @throws \RangeException The given length is less than 1.
	 */
	private static function split(
		string $path,
		int $length,
		string $separator = null
	): string {

		if ($length < 1) {
			throw new RangeException('Length must be greater than or equal to 1');
		}

		if (is_null($separator)) {
			$separator = DIRECTORY_SEPARATOR;
		}

		return implode($separator, str_split($path, $length));
	}

	private static function toFileSystem(
			int $buffer = null,
			string $type = null
	) {

		if (is_null($buffer)) {
			$buffer = \ShrooPHP\RESTful\Collections\FileSystem::BUFFER;
		}

		return new class($buffer, $type) implements Collection {

			private $buffer;

			private $type;

			use FileSystem {
				get as convert;
			}

			public function __construct(int $buffer, string $type = null)
			{
				$this->buffer = $buffer;
				$this->type = $type;
			}

			public function get(string $id): ?Resource
			{
				$resource = $this->convert($id);

				if (!(is_null($resource) || is_null($this->type))) {
					$resource = $this->override($resource, $this->type);
				}

				return $resource;
			}

			protected function buffer(): int
			{
				return $this->buffer;
			}

			private function override(
					Resource $resource,
					string $type
			): Resource {

				return new class($resource, $type) implements Resource {

					private $resource;

					private $type;

					public function __construct(
							Resource $resource,
							string $type
					) {
						$this->resource = $resource;
						$this->type = $type;
					}

					public function modified(): \DateTime
					{
						return $this->resource->modified();
					}

					public function render(
							int $start = 0,
							int $length = null
					): void {
						$this->resource->render($start, $length);
					}

					public function size(): ?int
					{
						return $this->resource->size();
					}

					public function type(): ?string
					{
						return $this->type;
					}
				};
			}
		};
	}
}
