# JontyNewman\Oku\File

Functionality for assisting with persistence via a file system for
[JontyNewman\Oku](https://gitlab.com/jontynewman/oku).

## Installation

```
composer require 'jontynewman/oku-file ^1.0'
```

