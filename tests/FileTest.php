<?php

namespace JontyNewman\Oku\Tests;

use DateInterval;
use DateTime;
use JontyNewman\Oku\File;
use JontyNewman\Oku\ResponseBuilders\RequestHandlerResponseBuilder;
use PHPUnit\Framework\TestCase;
use RangeException;
use UnexpectedValueException;

class FileTest extends TestCase
{
	public function testBuild()
	{
		$this->assertBuild(null, true, $this->content());
	}

	public function testBuildRange()
	{
		$size = 0x10;
		$end = 2 * $size;
		$total = $this->size();
		$request = ['Range' => "bytes={$size}-{$end}"];
		$headers = $this->headers();
		$content = substr($this->content(), $size, $size);

		$headers['Content-Range'] = "bytes {$size}-{$end}/{$total}";
		$headers['Content-Length'] = $size;

		$this->assertBuild(
				null,
				true,
				$content,
				null,
				$headers,
				null,
				$request
		);
	}

	public function testBuildUnmodified()
	{
		$keys = ['Pragma', 'Cache-Control', 'Last-Modified'];
		$filled = array_fill_keys($keys, null);
		$headers = array_intersect_key($this->headers(), $filled);
		$since = $this->modified()->format(DateTime::RFC2822);
		$request = ['If-Modified-Since' => $since];

		$this->assertBuild(null, true, null, 304, $headers, null, $request);
	}

	public function testBuildType()
	{
		$type = 'text/css';
		$headers = $this->headers();
		$headers['Content-Type'] = $type;
		$this->assertBuild(
				null,
				true,
				$this->content(),
				null,
				$headers,
				$type
		);
	}

	public function testBuildWithNonexistentFile()
	{
		$this->assertBuild('', false, null, null, []);
	}

	public function testPath()
	{
		$path = 'path/to/file';
		$this->assertPath($this->encode($path), $path);
	}

	public function testPathMax()
	{
		$path = 'path/to/file';
		$max = 2;
		$encoded = $this->encode($path);
		$expected = implode(DIRECTORY_SEPARATOR, str_split($encoded, $max));

		$this->assertPath($expected, $path, $max);
	}

	public function testPathSeparator()
	{
		$path = 'path/to/file';
		$max = 2;
		$separator = '|';
		$encoded = $this->encode($path);
		$expected = implode($separator, str_split($encoded, $max));

		$this->assertPath($expected, $path, $max, $separator);
	}

	public function testPathFailure()
	{
		$exception = null;
		$path = '';

		try {
			File::path($path, null, null, true);
		} catch (UnexpectedValueException $exception) {
			$expected = "Failed to encode path '{$path}'";
			$actual = $exception->getMessage();
			$this->assertEquals($expected, $actual);
		}

		$this->assertNotNull($exception);
	}

	public function testPathReliability()
	{
		$exception = null;
		$path = '';

		try {
			File::path($path);
		} catch (UnexpectedValueException $exception) {
			$expected = "Could not reliably encode path '{$path}'";
			$actual = $exception->getMessage();
			$this->assertEquals($expected, $actual);
		}

		$this->assertNotNull($exception);
	}

	public function testPathChunkSize()
	{

		$exception = null;

		try {
			File::path('=', 0);
		} catch (RangeException $exception) {
			$expected = 'Length must be greater than or equal to 1';
			$actual = $exception->getMessage();
			$this->assertEquals($expected, $actual);
		}

		$this->assertNotNull($exception);
	}

	public function testResolve()
	{
		$path = 'path/to/file';
		$this->assertSame($path, File::resolve(File::path($path)));
	}

	public function testResolveReliability()
	{
		$name = '!';
		$exception = null;

		try {
			File::resolve($name);
		} catch (UnexpectedValueException $exception) {
			$expected = "Could not reliably decode name '{$name}'";
			$actual = $exception->getMessage();
			$this->assertEquals($expected, $actual);
		}

		$this->assertNotNull($exception);
	}

	private function assertBuild(
		string $path = null,
		bool $built = true,
		string $content = null,
		int $code = null,
		array $headers = null,
		string $type = null,
		array $request = null
	): void {

		$builder = new RequestHandlerResponseBuilder();

		if (is_null($path)) {
			$path = __FILE__;
		}

		if (is_null($headers)) {
			$headers = $this->headers();
		}

		$this->assertEquals($built, File::build($builder, $path, $request, null, $type));

		$response = $builder->response();

		$this->assertSame($code, $response->code());

		$this->assertSame($headers, iterator_to_array($response->headers()));

		$actual = $response->content();

		if (is_null($content)) {

			$this->assertNull($actual);

		} else {

			$this->assertNotNull($actual);

			ob_start();
			$actual->run();
			$this->assertEquals($content, ob_get_clean());
		}
	}

	private function assertPath(
		string $expected,
		string $path,
		int $max = null,
		string $separator = null
	): void {

		$this->assertEquals($expected, File::path($path, $max, $separator));
	}

	private function headers(): array
	{
		$size = $this->size();
		$modified = $this->modified();

		$finfo = finfo_open(FILEINFO_MIME);
		$type = finfo_file($finfo, __FILE__);

		finfo_close($finfo);

		return [
			'Pragma' => 'no-cache',
			'Cache-Control' => 'no-cache',
			'Last-Modified' => $modified->format('D, d M Y H:i:s \G\M\T'),
			'Accept-Ranges' => 'bytes',
			'Content-Range' => "bytes 0-*/{$size}",
			'Content-Type' => $type,
		];
	}

	private function size(): int
	{
		return filesize(__FILE__);
	}

	private function modified(): DateTime
	{
		return DateTime::createFromFormat('U', filemtime(__FILE__));
	}

	private function content(): string
	{
		return file_get_contents(__FILE__);
	}

	private function encode(string $string): string
	{
		return rtrim(base64_encode($string), '=');
	}
}
